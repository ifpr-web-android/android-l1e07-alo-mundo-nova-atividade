package com.example.everaldo.l1x7alomundonovatelaactivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class AloPessoaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alo_pessoa);

        Intent intent = getIntent();
        String nomePessoa = intent.getStringExtra(MainActivity.NOME_PESSOA);

        TextView cumprimenta = (TextView) findViewById(R.id.cumprimenta);
        cumprimenta.setText(alo(nomePessoa));
    }

    private String alo(String nomePessoa){
        return "Alô, " + nomePessoa + "!";
    }

}
