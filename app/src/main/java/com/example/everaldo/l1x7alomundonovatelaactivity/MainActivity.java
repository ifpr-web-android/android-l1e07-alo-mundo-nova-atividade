package com.example.everaldo.l1x7alomundonovatelaactivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    public static final String NOME_PESSOA = "NOME_PESSOA";
    private final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button novaAtividade = (Button) findViewById(R.id.botaoNovaAtividade);

        novaAtividade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nomePessoa = ((EditText)findViewById(R.id.nomePessoa)).getText().toString();
                Log.d(TAG, nomePessoa);
                Intent intent = new Intent(MainActivity.this, AloPessoaActivity.class);
                intent.putExtra(NOME_PESSOA, nomePessoa);
                startActivity(intent);


            }
        });

    }
}
